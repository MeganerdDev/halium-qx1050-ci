#!/bin/sh

# setup build env
sudo zypper -n in lvm2 atruncate pigz
sudo zypper in -y kmod 
sudo zypper rm droid-tools || true # it's ok if you hadn't any
sudo zypper -n in android-tools

echo "Creating loop devices..."
sudo mknod /dev/loop0 -m0660 b 7 0
sudo mknod /dev/loop1 -m0660 b 7 1
sudo mknod /dev/loop2 -m0660 b 7 2
echo $?
ls -lh /dev/loop*

echo "Building Sailfish $RELEASE"

sudo mic create loop --arch=$PORT_ARCH \
--tokenmap=ARCH:$PORT_ARCH,RELEASE:$RELEASE,RNDRELEASE:latest,EXTRA_NAME:$EXTRA_NAME \
--record-pkgs=name,url \
--outdir=sfe-$DEVICE-$RELEASE$EXTRA_NAME \
Jolla-@RELEASE@-$DEVICE-@ARCH@.ks
